
PV = "next-20241216+git"

SRCREV = "next-20241216"

KERNEL_GIT_BRANCH = "tag=${SRCREV};nobranch=1"

FILESEXTRAPATHS:prepend := "${THISDIR}/files:"

KERNEL_REPRODUCIBILITY_PATCHES = " \
    file://0001-drivers-gpu-drm-msm-registers-improve-reproducibilit.patch \
    file://0001-perf-python-Fix-compile-for-32bit-platforms.patch \
    file://0001-arm-Remove-build-path-from-generated-mach-types.h.patch \
"

do_install:prepend() {
    mkdir -p ${D}${nonarch_base_libdir}/modules/${KERNEL_VERSION}
    touch ${D}${nonarch_base_libdir}/modules/${KERNEL_VERSION}/source
}

