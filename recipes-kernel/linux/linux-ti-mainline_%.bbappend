
FILESEXTRAPATHS:prepend := "${THISDIR}/files:"

include ${@ 'recipes-kernel/linux/ti-kernel-devicetree-prefix.inc' if d.getVar('KERNEL_DEVICETREE_PREFIX') else ''}

PV = "6.14-rc3+git"

SRCREV = "v6.14-rc3"

KERNEL_GIT_BRANCH = "tag=${SRCREV};nobranch=1"

KERNEL_REPRODUCIBILITY_PATCHES = " \
    file://0001-drivers-gpu-drm-msm-registers-improve-reproducibilit.patch \
    file://0001-perf-python-Fix-compile-for-32bit-platforms.patch \
    file://0001-arm-Remove-build-path-from-generated-mach-types.h.patch \
"

do_install:prepend() {
    mkdir -p ${D}${nonarch_base_libdir}/modules/${KERNEL_VERSION}
    touch ${D}${nonarch_base_libdir}/modules/${KERNEL_VERSION}/source
}

do_deploy:append() {
    echo "${KERNEL_DEVICETREE}" > ${DEPLOYDIR}/kernel-devicetree.txt
}

