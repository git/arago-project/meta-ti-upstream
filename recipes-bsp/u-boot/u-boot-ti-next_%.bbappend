
SRC_URI:remove = "file://0001-scripts-dtc-pylibfdt-libfdt.i_shipped-Use-SWIG_Appen.patch"

SRCREV = "8aab7730c1c26192b0e4218fbf2f6009f955cb88"

BRANCH = "next"

DEPENDS += "gnutls-native"

do_install:append:am62xx() {
	install -d ${D}/boot
	install -m 0644 ${S}/tools/logos/ti_logo_414x97_32bpp.bmp.gz ${D}/boot
}

do_deploy:append:am62xx() {
	install -d ${DEPLOYDIR}
	install -m 0644 ${S}/tools/logos/ti_logo_414x97_32bpp.bmp.gz ${DEPLOYDIR}
}

UBOOT_MACHINE:beagleplay:bsp-next = "am62x_beagleplay_a53_defconfig"
UBOOT_CONFIG_FRAGMENTS:beagleplay:bsp-next = ""

UBOOT_MACHINE:beagleplay-k3r5:bsp-next = "am62x_beagleplay_r5_defconfig"
UBOOT_CONFIG_FRAGMENTS:beagleplay-k3r5:bsp-next = ""

