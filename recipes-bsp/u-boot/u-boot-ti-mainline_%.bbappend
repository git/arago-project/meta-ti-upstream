
PV = "2025.01+git"

SRC_URI:remove = "file://0001-scripts-dtc-pylibfdt-libfdt.i_shipped-Use-SWIG_Appen.patch"

SRCREV = "v2025.01"

UBOOT_GIT_BRANCH = "tag=${SRCREV};nobranch=1"

do_install:append:am62xx() {
	install -d ${D}/boot
	install -m 0644 ${S}/tools/logos/ti_logo_414x97_32bpp.bmp.gz ${D}/boot
}

do_deploy:append:am62xx() {
	install -d ${DEPLOYDIR}
	install -m 0644 ${S}/tools/logos/ti_logo_414x97_32bpp.bmp.gz ${DEPLOYDIR}
}

UBOOT_MACHINE:beagleplay:bsp-mainline = "am62x_beagleplay_a53_defconfig"
UBOOT_CONFIG_FRAGMENTS:beagleplay:bsp-mainline = ""

UBOOT_MACHINE:beagleplay-k3r5:bsp-mainline = "am62x_beagleplay_r5_defconfig"
UBOOT_CONFIG_FRAGMENTS:beagleplay-k3r5:bsp-mainline = ""

