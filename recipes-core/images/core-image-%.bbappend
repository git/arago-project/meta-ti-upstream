
IMAGE_FSTYPES:append = " cpio.xz"

PACKAGE_CLASSES = "package_ipk"

EXTRA_IMAGE_FEATURES:append = " package-management"

IMAGE_INSTALL:append = " \
    inetutils \
    inetutils-telnetd \
    systemd-telnetd \
    ti-test \
    xz \
"

